# small book reader

This is a public domain (CC0) set of tools (mainly Python3 and C) for creating small and simple executable plain text books for different platforms, including small embedded consoles.

So far these platforms are supported:
- terminal
- SDL2
- Pokitto
- Arduboy
- Gamebuino META

![](photo.jpg)

## how to use

If you want to create a book for supported platform:

1. Have a text with the book in plain text format, most preferably ASCII.
2. Run `python3 process.py booktext.txt platform`. This will create a folder *output* with files you can compile to get the book (will likely not work under Windows, some Bash commands are used, but nobody uses Windows anyway). A file named `preview.txt` will be created in which you can check the output formatting.

If you want to add a new platform:

1. Create a new entry at the top of `process.py` script for YOURPLATFORM.
2. Write a new front end, i.e. `platform_YOURPLATFORM.h`. Take a look at already existing front ends to get an idea how this is done. Basically:
  - Your platform handles the program flow (i.e. main loop etc.) and uses the provided core functions.
  - You have to call `BRD_init()` at the beginning, then you can use functions like `BRD_nextPage()` etc.
  - Your front end can do whatever extra thing it supports, such as bookmark saving, changing colors and fonts at runtime, reading text aloud etc. That is up to you.
  - There are basically two ways you can handle displaying the text:
      1. If your platform supports rendering text, the preferred way is you reading the page text from `BRD_pageArray` and rendering it yourself.
      2. In case you can't render text, the core provides a very simple function and a 4x4 bitmap font for this. Just clear screen and call `BRD_renderPage` (which takes a pointer to a function that your code uses to set individual pixels).

## compression

The framework uses a simple compression that achieves about 50% size reduction of the text. The compression is performed by the `process.py` script that creates the book data. It works in the following way:

- Characters of text are 7bit ASCII stored as 8bit bytes so we have 128 unused values because of the extra unused bit (actually more if we exclude invisible characters of the ASCII table). We use these (except value 255 reserved for page separator) as indices to a dictionary of byte pairs. So we are able to replace most common byte pairs (which may contain both ASCII characters and references to the dictionary) in the text with a single byte and one entry in the dictionary. **This can be done repeatedly** on data that already contain references to the dictionary.
- The compressed book data therefore consist of the dictionary of byte pairs plus the compressed byte data themselves. To decompress we simply keep recursively expanding the dictionary references (non-terminals) until we have only ASCII characters (terminals) and can replace no more.
- The book text is preprocessed before compression, e.g. putting line breaks to fit given platform's screen size, converting Unicode to ASCII etc.
- As part of preprocessing the text is split into pages (depending on platform's screen size in characters) and **each page is compressed independently** so that it is possible to seek and decompress only given page at runtime (on platforms with little RAM we can't decompress the whole text). This is achieved by inserting page separators into compressed text (byte value 255), and during compression ignoring byte pairs that contain this separator. Seeking pages in compressed data can simply be done by counting the separators.
- If compression doesn't achieve size reduction, it is not performed (format of the data stays the same, but the dictionary is left empty, the compressed data only contain terminals).

## license

Everything in this repository is completely pubic domain under CC0 1.0. I have created and written everything myself or used public domain resources (e.g. books whose authors have died more than 100 years ago).